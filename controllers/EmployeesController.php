<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\models\Employees;
use app\models\Companies;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

class EmployeesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['get'],
                    'create' => ['get'],
                    'store' => ['post'],
                    'view' => ['get'],
                    'update' => ['get'],
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        $arrUserPermission = ['admin@admin.com'];
        if(!Yii::$app->user->isGuest && !in_array(Yii::$app->user->identity->email, $arrUserPermission)) {
            throw new \yii\web\HttpException(403, "You're not allowed for this action.");
        }

        return parent::beforeAction($action);
    }

    public function actionIndex() {
        $searchModel = new Employees();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $filterCompanies = Companies::find()->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'filterCompanies' => ArrayHelper::map($filterCompanies, 'id', 'name')
        ]);
    }

    public function actionCreate() {
        $model = new Employees();
        $filterCompanies = Companies::find()->all();

        return $this->render('create', [
            'model' => $model,
            'filterCompanies' => ArrayHelper::map($filterCompanies, 'id', 'name')
        ]);
    }

    public function actionStore() {
        $model = new Employees();

        if ($model->load(Yii::$app->request->post())) {
            if(isset(Yii::$app->request->post('Employees')['id'])) {
                // update data Employees
                $model = Employees::find()->where(['id' => Yii::$app->request->post('Employees')['id']])->one();

                $model->first_name = Yii::$app->request->post('Employees')['first_name'];
                $model->last_name = Yii::$app->request->post('Employees')['last_name'];
                $model->company_id = Yii::$app->request->post('Employees')['company_id'];
                $model->email = (Yii::$app->request->post('Employees')['email'] == "") ? NULL : Yii::$app->request->post('Employees')['email'];
                $model->phone = (Yii::$app->request->post('Employees')['phone'] == "") ? NULL : Yii::$app->request->post('Employees')['phone'];
            } else {
                // TO CREATE EMPLOYEES
                $model->email = ($model->email == "") ? NULL : $model->email;
                $model->phone = ($model->phone == "") ? NULL : $model->phone;
            }

            if($model->save()) {
                Yii::$app->session->setFlash('success', 'Employee created successfully');
                return $this->redirect('index');
            } else {
                $filterCompanies = Companies::find()->all();
                Yii::$app->session->setFlash('error', 'There is something wrong, please try again');
                $action = (!isset(Yii::$app->request->post('Employees')['id'])) ? 'create' : 'update';
                return $this->render($action, ['model' => $model, 'filterCompanies' => ArrayHelper::map($filterCompanies, 'id', 'name')]);
            }
        } else {
            Yii::$app->session->setFlash('error', 'There is something wrong, please try again');
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionView($id){
        $model = Employees::find()->select(['*', 'CONCAT(first_name, " ", last_name) full_name'])->where(['id' => $id])->one();

        if($model == null || empty($model)) {
            throw new NotFoundHttpException('Employee does not exist.');
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = Employees::find()->where(['id' => $id])->select(['*', 'CONCAT(first_name, " ", last_name) full_name'])->one();
        $filterCompanies = Companies::find()->all();

        return $this->render('update', [
            'model' => $model,
            'filterCompanies' => ArrayHelper::map($filterCompanies, 'id', 'name')
        ]);
    }

    public function actionDelete($id) {
        $delete = Employees::find()->where(['id' => $id])->one()->delete();
        if($delete) {
            Yii::$app->session->setFlash('success', 'Employee deleted successfully');
        } else {
            Yii::$app->session->setFlash('error', 'There is something wrong, please try again');
        }

        return $this->redirect(['index']);
    }
}
