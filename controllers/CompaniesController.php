<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\models\Companies;
use app\models\Employees;
use yii\web\UploadedFile;

class CompaniesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['get'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        $arrUserPermission = ['admin@admin.com'];
        if(!Yii::$app->user->isGuest && !in_array(Yii::$app->user->identity->email, $arrUserPermission)) {
            throw new \yii\web\HttpException(403, "You're not allowed for this action.");
        }

        return parent::beforeAction($action);
    }

    public function actionIndex() {
        $searchModel = new Companies();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new Companies();

        return $this->render('create', [
            'model' => $model
        ]);
    }

    public function actionStore() {
        $model = new Companies();

        if ($model->load(Yii::$app->request->post())) {
            $file_name = "";
            $current_logo = "";
            if(isset(Yii::$app->request->post('Companies')['id'])) {
                // update data companies
                $model = Companies::find()->where(['id' => Yii::$app->request->post('Companies')['id']])->one();
                $current_logo = $model->logo;

                $model->name = Yii::$app->request->post('Companies')['name'];
                $model->email = (Yii::$app->request->post('Companies')['email'] == "") ? NULL : Yii::$app->request->post('Companies')['email'];
                $model->website = Yii::$app->request->post('Companies')['website'];
            } else {
                $model->email = ($model->email == "") ? NULL : $model->email;
            }

            $logoFile = UploadedFile::getInstance($model, 'logo');
            if(!is_null($logoFile)) {
                $companyName = 'logo_'.str_replace(" ", "_", $model->name);
                $file_name = 'upload_image/' . $companyName . '.' . $logoFile->extension;
                $model->logo = "/".$file_name;
            } else {
                $model->logo = ($current_logo != "") ? $current_logo : "";
            }

            if($model->save()) {
                if(!is_null($logoFile)) {
                    $companyName = 'logo_'.str_replace(" ", "_", $model->name);
                    $file_name = 'upload_image/' . $companyName . '.' . $logoFile->extension;
                    $logoFile->saveAs($file_name);
                }

                Yii::$app->session->setFlash('success', 'Company created successfully');
                return $this->redirect('index');
            } else {
                Yii::$app->session->setFlash('error', 'There is something wrong, please try again');
                $model->logo = ($current_logo != "") ? $current_logo : "";
                $action = (!isset(Yii::$app->request->post('Companies')['id'])) ? 'create' : 'update';
                return $this->render($action, ['model' => $model]);
            }
        } else {
            Yii::$app->session->setFlash('error', 'There is something wrong, please try again');
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionView($id){
        $model = Companies::find()->where(['id' => $id])->one();

        if($model == null || empty($model)) {
            throw new NotFoundHttpException('Company does not exist.');
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = Companies::find()->where(['id' => $id])->one();

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        $data = Companies::find()->where(['id' => $id])->one();
        $url_image = $data->logo;

        $delete = $data->delete();
        if($delete) {
            if (!is_null($url_image) && $url_image != "" && file_exists(Yii::$app->basePath . '/web/' . $url_image)) {
                unlink(Yii::$app->basePath . '/web/' . $url_image);
            }

            Yii::$app->session->setFlash('success', 'Company deleted successfully');
        } else {
            Yii::$app->session->setFlash('error', 'There is something wrong, please try again');
        }

        return $this->redirect(['index']);
    }

    public function actionCheckEmployee() {
        $data = Employees::find()
                ->where(['company_id' => $_POST['company_id']])
                ->count();

        $return = ['count' => $data];

        return $this->asJson($return);
    }
}
