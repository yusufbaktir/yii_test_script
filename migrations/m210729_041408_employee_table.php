<?php

use yii\db\Migration;

/**
 * Class m210729_041408_employee_table
 */
class m210729_041408_employee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('employees', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'email' => $this->string()->unique(),
            'phone' => $this->string(15)->unique()
        ]);

        $this->createIndex(
            'idx-employees-company_id',
            'employees',
            'company_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-employees-company_id',
            'employees',
            'company_id',
            'companies',
            'id',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-employees-company_id',
            'employees'
        );

        $this->dropIndex(
            'idx-employees-company_id',
            'employees'
        );

        $this->dropTable('employees');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210729_041408_employee_table cannot be reverted.\n";

        return false;
    }
    */
}
