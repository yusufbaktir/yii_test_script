<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m210730_101038_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->unique(),
            'password' => $this->string()->unique()
        ]);

        // $this->insert('{{%user}}', 
        //     [
        //         'email' => 'admin@admin.com',
        //         'password' => \Yii::$app->security->generatePasswordHash('password'),
        //     ],
        //     [
        //         'email' => 'user@user.com',
        //         'password' => \Yii::$app->security->generatePasswordHash('password'),
        //     ]
        // );

        Yii::$app->db->createCommand()->batchInsert('{{%user}}', ['email', 'password'], [
            ['admin@admin.com', \Yii::$app->security->generatePasswordHash('password')],
            ['user@user.com', \Yii::$app->security->generatePasswordHash('password')],
        ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
