<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

class Employees extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'employees';
    }

    /**
     * {@inheritdoc}
     */
    public $full_name;
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'company_id'], 'required'],
            [['id'], 'integer'],
            [['full_name'], 'safe'],
            [['phone'], 'string', 'max' => 15],
            [['email', 'first_name', 'last_name'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['phone', 'email'], 'unique'],
            [['phone'], 'match' ,'pattern'=>'/^[0-9]+$/u', 'message'=> 'Contact No can Contain only numeric characters.'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'company_id' => 'Company',
            'email' => 'Email',
            'phone' => 'Phone'
        ];
    }

    public function getCompanies() {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    public function search($params)
    {
        $this->load($params);
        
        $query = Employees::find()
                 ->select(['*', 'CONCAT(first_name, " ", last_name) full_name']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 2,
            ],
        ]);

        $dataProvider->sort->attributes['full_name'] = [
            'asc' => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC],
            'desc' => ['first_name' => SORT_DESC, 'last_name' => SORT_DESC],
        ];

        $query->andFilterWhere([
            'company_id' => $this->company_id,
        ]);

        $query->andFilterWhere(['like', 'CONCAT(first_name, " ", last_name)', $this->full_name])
              ->andFilterWhere(['like', 'email', $this->email])
              ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }
}
