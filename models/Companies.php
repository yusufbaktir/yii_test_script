<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

class Companies extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'companies';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['id'], 'integer'],
            [['website'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['name', 'email'], 'unique'],
            [['logo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxSize' => 1024 * 1024 * 1]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'logo' => 'Logo',
            'website' => 'Website'
        ];
    }

    public function getEmployees() {
        return $this->hasMany(Employees::className(), ['company_id' => 'id']);
    }

    public function search($params)
    {
        $this->load($params);
        
        $query = Companies::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 2,
            ],
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
              ->andFilterWhere(['like', 'email', $this->email])  
              ->andFilterWhere(['like', 'logo', $this->logo])
              ->andFilterWhere(['like', 'website', $this->website]);

        return $dataProvider;
    }
}
