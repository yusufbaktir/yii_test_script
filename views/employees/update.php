<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\select2\Select2;

$this->title = 'Update: '.$model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Employees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<script type="text/javascript">
    window.onload = function() {
        $( "#submit_form" ).submit(function( event ) {
            if($('#employees-first_name').val() != "" && $('#employees-last_name').val() != "" && $('#employees-company_id option:selected').val() != "") {
                $('.btn-save').html("<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Processing...");
                $('.btn-save').prop('disabled', true);
            }
        });
    }

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 43) {
            return false;
        }
        return true;
    }
</script>

<div>
	<h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(['id' => 'submit_form', 'action' => ['employees/store']]); ?>

    <?= Html::hiddenInput('Employees[id]', $model->id) ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?=
        $form->field($model, 'company_id')->widget(Select2::classname(), [
            'data' => $filterCompanies,
            'options' => ['placeholder' => 'Select a Company...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('Company');
    ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true])->input('email') ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true, "onkeypress" => "return isNumber(event)"]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-save']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
