<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;

$this->title = 'Employees';
$this->params['breadcrumbs'][] = $this->title;

$value_companies_id = "";
if(isset($_GET['Employees']['company_id'])){
    $value_companies_id = $_GET['Employees']['company_id'];
}
?>

<script type="text/javascript">
    window.onload = function() {
        $( "#delete_form" ).submit(function( event ) {
            $('.btn-delete').html("<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Processing...");
            $('.btn-delete').prop('disabled', true);
            $('.btn-cancel-delete').prop('disabled', true);
        });
    }
    function showModalDelete(id, full_name) {
        $("#modal").modal('show');
        $('#delete_form').attr('action', $('#delete_form').attr('action') + "?id="+id);
        $('.label-desc').html('Employee that are deleted cannot be returned. Are you sure you want to delete '+full_name+'?');
    }
</script>

<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php 
            echo Html::a('Create Employees', ['create'], ['class' => 'btn btn-success']);
        ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'full_name',
            [
                'attribute' => 'company_id',
                'value' => 'companies.name',
                'filter' => Select2::widget([
                    'name' => 'Employees[company_id]',
                    'data' => $filterCompanies,
                    'value' => $value_companies_id,
                    'options' => ['placeholder' => 'Select a company ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])
            ],
            [
                'attribute' => 'email',
                'format' => 'email',
                'value' => function($model) {
                    return (is_null($model->email)) ? "" : $model->email;
                }
            ],
            [
                'attribute' => 'phone',
                'format' => 'html',
                'value' => function($model) {
                    return (is_null($model->phone)) ? "" : Html::a($model->phone, 'tel:'.$model->phone);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons'=>[
                    'view'=>function ($url, $model) {
                        return  Html::a('<span class="glyphicon glyphicon-eye-open"></span>', 
                                        Url::to(['employees/view', 'id' => $model->id]), 
                                        ['class' => 'btn btn-primary btn-xs']
                                );
                    },
                    'update'=>function ($url, $model) {
                        return  Html::a('<span class="glyphicon glyphicon-pencil"></span>', 
                                        Url::to(['employees/update', 'id' => $model->id]), 
                                        ['class' => 'btn btn-warning btn-xs']
                                );
                    },
                    'delete'=>function ($url, $model) {
                        return  Html::button('<span class="glyphicon glyphicon-trash"></span>', 
                                            [
                                                'onclick'=> 'showModalDelete("'.$model->id.'", "'.$model->full_name.'");',
                                                'data-toggle'=>'tooltip',
                                                'title'=>Yii::t('app', 'Delete'),
                                                'class' => 'btn btn-danger btn-xs'
                                            ]
                                );
                    },
                ],
            ]
        ],
    ]); ?>


</div>

<?php
Modal::begin([
    'header' => '<h3 style="color:red;">WARNING!</h3>',
    'id' => 'modal',
    'size' => 'modal-md',
]);

ActiveForm::begin(['action' => ['employees/delete'], 'id' => 'delete_form', 'method' => 'post', 'layout' => 'horizontal', ]); ?>
<?= Html::csrfMetaTags() ?>
<div class='modal-body'>
    <p class="label-desc" style="font-weight: normal;"></p>
</div>
<div class="modal-footer footer-delete">
    <div class='form-group'>
        <button class='btn btn-danger btn-delete'><span class='glyphicon glyphicon-trash'></span> Yes</button>
        <button type="button" class="btn btn-success btn-cancel-delete" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"></span> No</button>
    </div>
</div>
<?php
ActiveForm::end();
Modal::end();
?>
