<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

$this->title = $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Employees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="bomanager-purchasing-order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'full_name',
            [
                'attribute' => 'company_id',
                'value' => $model->companies->name,
            ],
            [
                'attribute' => 'email',
                'format' => 'email',
                'value' => (is_null($model->email)) ? "" : $model->email,
            ],
            [
                'attribute' => 'phone',
                'format' => 'html',
                'value' => (is_null($model->phone)) ? "" : Html::a($model->phone, 'tel:'.$model->phone),
            ]
        ],
    ]) ?>

</div>
