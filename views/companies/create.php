<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BomanagerMasterSupplier */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Create Company';
$this->params['breadcrumbs'][] = ['label' => 'Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<script type="text/javascript">
    window.onload = function() {
        $( "#submit_form" ).submit(function( event ) {
            if($('#companies-name').val() != "" && ($("#companies-logo")[0].files.length == 0 || $("#companies-logo")[0].files[0].size <= 1000000)) {
                $('.btn-save').html("<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Processing...");
                $('.btn-save').prop('disabled', true);
            }
        });
    }
</script>

<div>
	<h1><?= Html::encode($this->title) ?></h1>
	
    <?php $form = ActiveForm::begin(['id' => 'submit_form', 'action' => ['companies/store'], 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true])->input('email') ?>

    <?= $form->field($model, 'logo')->fileInput() ?>

    <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-save']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
