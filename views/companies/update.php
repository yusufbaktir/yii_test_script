<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\BomanagerMasterSupplier */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Update: '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<script type="text/javascript">
    window.onload = function() {
        $( "#submit_form" ).submit(function( event ) {
            if($('#companies-name').val() != "") {
                $('.btn-save').html("<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Processing...");
                $('.btn-save').prop('disabled', true);
            }
        });
    }
</script>

<div>
	<h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(['id' => 'submit_form', 'action' => ['companies/store'], 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= Html::hiddenInput('Companies[id]', $model->id) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true])->input('email') ?>

    <div class="form-group">
    	<div>
    		<label class="control-label">Current Logo</label>
    	</div>
    	<div>
    		<?php if ($model->logo == "" || is_null($model->logo)): ?>
                <p class="text-danger">Logo has never been uploaded</p>
            <?php else : ?>
                <img src="<?php echo Url::base().$model->logo ?>" width=300 heigt=300>
            <?php endif ?>
    	</div>
    </div>

    <?= $form->field($model, 'logo')->fileInput(['value' => Url::base().$model->logo])->label('New Logo <smal>(only if you will change current logo)</small>') ?>

    <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-save']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
