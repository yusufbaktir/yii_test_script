<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="bomanager-purchasing-order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'email',
            [
                'attribute' => 'logo',
                'value' => Url::base().$model->logo . '?v=' . date("His"),
                'format' => ['image', ['width' => '300', 'height' => '300']],
            ],
            'website'
        ],
    ]) ?>

</div>
