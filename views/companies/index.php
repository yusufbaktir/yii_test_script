<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;

$this->title = 'Companies';
$this->params['breadcrumbs'][] = $this->title;
?>
<script type="text/javascript">
    window.onload = function() {
        $( "#delete_form" ).submit(function( event ) {
            $('.btn-delete').html("<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Processing...");
            $('.btn-delete').prop('disabled', true);
            $('.btn-cancel-delete').prop('disabled', true);
        });
    }
    function showModalDelete(id, name) {
        var url_check_employee = <?php echo "'" . Url::to(['companies/check-employee']) . "'" ?>;

        $.ajax({
            url: url_check_employee,
            type: "POST",
            dataType: "json",
            data: {company_id: id},
            beforeSend: function() {
                $(".btn-modal-delete").html("<span class=\"glyphicon glyphicon-refresh glyphicon-refresh-animate\"></span>");
                $(".btn-modal-delete").prop("disabled", true);
            },
            success:function(response_data_json) {
                console.log(response_data_json.count);
                $(".btn-modal-delete").html("<span class=\"glyphicon glyphicon-trash\"></span>");
                $(".btn-modal-delete").prop("disabled", false);

                var msg = "";
                if(response_data_json.count > 0) {
                    msg = name+' cannot be deleted because there are still connected employee data';
                    $('.footer-delete').hide();
                } else {
                    msg = 'Company that are deleted cannot be returned. Are you sure you want to delete '+name+'?';
                    $('.footer-delete').show();
                }
                
                $("#modal").modal('show');
                $('#delete_form').attr('action', $('#delete_form').attr('action') + "?id="+id);
                $('.label-desc').html(msg);
            }
        });
    }
</script>

<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php 
            echo Html::a('Create Company', ['create'], ['class' => 'btn btn-success']);
        ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'email',
                'value' => function($model) {
                    return (is_null($model->email)) ? "" : $model->email;
                }
            ],
            [
                'attribute' => 'logo',
                'format' => 'html',
                'label' => 'Logo',
                'filter' => false,
                'contentOptions' => ['style' => 'text-align:center;'],
                'value' => function ($data) {
                    $baseurl = Url::base();
                    return Html::img($baseurl.$data['logo'] . '?v=' . date("His"), ["style" => "height:100px"]);
                },
            ],
            [
                'attribute' => 'website',
                'value' => function($model) {
                    return (is_null($model->website)) ? "" : $model->website;
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons'=>[
                    'view'=>function ($url, $model) {
                        return  Html::a('<span class="glyphicon glyphicon-eye-open"></span>', 
                                        Url::to(['companies/view', 'id' => $model->id]), 
                                        ['class' => 'btn btn-primary btn-xs']
                                );
                    },
                    'update'=>function ($url, $model) {
                        return  Html::a('<span class="glyphicon glyphicon-pencil"></span>', 
                                        Url::to(['companies/update', 'id' => $model->id]), 
                                        ['class' => 'btn btn-warning btn-xs']
                                );
                    },
                    'delete'=>function ($url, $model) {
                        return  Html::button('<span class="glyphicon glyphicon-trash"></span>', 
                                            [
                                                'onclick'=> 'showModalDelete("'.$model->id.'", "'.$model->name.'");',
                                                'data-toggle'=>'tooltip',
                                                'title'=>Yii::t('app', 'Delete'),
                                                'class' => 'btn btn-danger btn-xs btn-modal-delete'
                                            ]
                                );
                    },
                ],
            ]
        ],
    ]); ?>


</div>

<?php
Modal::begin([
    'header' => '<h3 style="color:red;">WARNING!</h3>',
    'id' => 'modal',
    'size' => 'modal-md',
]);

ActiveForm::begin(['action' => ['companies/delete'], 'id' => 'delete_form', 'method' => 'post', 'layout' => 'horizontal', ]); ?>
<?= Html::csrfMetaTags() ?>
<div class='modal-body'>
    <p class="label-desc" style="font-weight: normal;"></p>
</div>
<div class="modal-footer footer-delete">
    <div class='form-group'>
        <button class='btn btn-danger btn-delete'><span class='glyphicon glyphicon-trash'></span> Yes</button>
        <button type="button" class="btn btn-success btn-cancel-delete" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"></span> No</button>
    </div>
</div>
<?php
ActiveForm::end();
Modal::end();
?>